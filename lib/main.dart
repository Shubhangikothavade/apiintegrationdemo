import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:news_api_demo/Model/article_model.dart';
import 'Services/GetAPI.dart';

void main() {
  runApp(MaterialApp(
    home: HomePage(),
  ));
}

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  //late Future<List<Article>> articles;

  @override
  void initState() {
    //articles = ClassgetAPI().getjsonFunction();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('API Demo'),
        ),
        body: FutureBuilder(
          future: ClassgetAPI().getjsonFunction(),
          builder:
              (BuildContext context, AsyncSnapshot<List<Article>> snapshot) {
            if (snapshot.hasData) {
              // print('in snapshot if : ${snapshot.data![0].url}');
              // print('in snapshot if : ${snapshot.data?.length}');
              return ListView.builder(
                  itemCount: snapshot.data?.length,
                  itemBuilder: (context, index) {
                    return GestureDetector(
                      onTap: () {
                        print("News api Clicked..");
                      },
                      child: Container(
                          height: 300,
                          child: Column(children: [
                            Image.network(
                              snapshot.data?[index].urlToImage as String,
                              fit: BoxFit.cover,
                              height: 200,
                              width: double.infinity,
                            ),
                            Text(
                              snapshot.data![index].url,
                              overflow: TextOverflow.ellipsis,
                              maxLines: 3,
                              style: TextStyle(
                                fontSize: 20,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ])),
                    );
                  });
            } else {
              return Text('don' 't have data');
            }
          },
        ));
  }
}