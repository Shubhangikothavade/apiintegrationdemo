import 'dart:convert';
import 'dart:core';
import 'package:http/http.dart' as http;

import 'package:news_api_demo/Model/article_model.dart';

class ClassgetAPI {
  Future<List<Article>> getjsonFunction() async {
    var client = http.Client();

    List<Article> articles = [];

    http.Response response = await client.get(Uri.parse(
        'https://newsapi.org/v2/top-headlines?country=in&apiKey=f3f408a1f9214b8b8bc9ac2eca256d96'));

    if (response.statusCode == 200) {

      Map<String, dynamic> jsonbody = json.decode(response.body);
      List<dynamic> body = jsonbody['articles'];

      // body.forEach((element) {
      //   print(element['url']);
      // });

      articles = body.map((dynamic item) => Article.fromJson(item)).toList();

      return articles;
    } else {
      throw ("Exception");
    }
  }
}
