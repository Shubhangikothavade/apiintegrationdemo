
class Source {
  Source({
     this.id,
     this.name,
  });

  String? id;
  String? name;

  factory Source.fromJson(Map<String, dynamic> json) => Source(
    id: json["id"],
    name: json["name"],
  );
  // factory Source.fromJson(Map<String, dynamic> json) => Source(
  //   id: json["id"]== null ? null : json["id"],
  //   name: json["name"]== null ? "" : json["name"],
  // );
}