import 'package:news_api_demo/Model/source_model.dart';

class Article {
  Article({
     this.source,
     this.author,
    required this.title,
     this.description,
    required this.url,
     this.urlToImage,
     this.publishedAt,
     this.content,
  });

  Source? source;
  String? author;
  String title;
  String? description;
  String url;
  String? urlToImage;
  String? publishedAt;
  String? content;

  factory Article.fromJson(Map<String, dynamic> json) => Article(
  source: Source.fromJson(json["source"]),
  author: json["author"],
  title: json["title"],
  description: json["description"],
  url: json["url"],
  urlToImage: json["urlToImage"] == null ? 'https://cdn.pixabay.com/photo/2015/09/16/08/55/online-942406_960_720.jpg' : json["urlToImage"],
  publishedAt: json["publishedAt"],
  content: json["content"],
  );
}